# streamsets-chrome-processor

This processor uses headless Chrome to make page requests.

* input: `/url`, e.g. http://www.example.com
* output: `/pageSource`, i.e. the HTML for that page


The Chrome browser and Chromedriver must be installed on the host(s) that run this processor.


